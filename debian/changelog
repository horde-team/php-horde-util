php-horde-util (2.5.12-2) unstable; urgency=medium

  * Team upload.

  [ Mike Gabriel ]
  * d/control: Bump Standards-Version: to 4.6.2. No changes needed.

  [ Anton Gladky ]
  * d/patches: Fix many warnings due to PHP8.2. (Closes: #1028952)
  * d/patches: Disable three tests. Should be fixed by upstream.

 -- Anton Gladky <gladk@debian.org>  Tue, 24 Jan 2023 20:27:42 +0100

php-horde-util (2.5.12-1) unstable; urgency=medium

  * New upstream version 2.5.12
  * d/patches: Add 1002_PHP-8.1.patch. Amend deprecation warnings during unit
    tests with PHP 8.1.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 03 Jul 2022 21:09:52 +0200

php-horde-util (2.5.9-2) unstable; urgency=medium

  * d/control: Drop optional dependencies from package.xml to Suggests: field.
    (Closes: #870871).
  * d/t/control: Require PHPUnit-8.x-ported php-horde-test.
  * d/patches: Add 1001_PHPUnit-8.x.patch. Adapt test cases to PHPUnit 8.x.
  * d/t/control: Add D: php-intl, locales-all (instead of locales).
  * d/t/: Drop needs-root restriction. Don't render locales for individual
    languages anymore.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 17 Jul 2020 00:59:05 +0200

php-horde-util (2.5.9-1) unstable; urgency=medium

  [ Mike Gabriel ]
  * New upstream version 2.5.9
  * d/copyright: Update copyright attributions.
  * d/tests/control: Drop deprecated needs-recommends restriction.
  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.

  [ Juri Grabowski ]
  * d/salsa-ci.yml: enable aptly

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 12:36:10 +0200

php-horde-util (2.5.8-5) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959287).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/copyright: Update copyright attributions.
  * d/upstream/metadata: Add file. Comply with DEP-12.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 04 May 2020 21:58:00 +0200

php-horde-util (2.5.8-4) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * salsa-ci.yml: Allow autopkgtest failure
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 20:33:45 +0200

php-horde-util (2.5.8-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 09:44:43 +0200

php-horde-util (2.5.8-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Thu, 05 Apr 2018 14:44:53 +0200

php-horde-util (2.5.8-1) unstable; urgency=medium

  * New upstream version 2.5.8

 -- Mathieu Parent <sathieu@debian.org>  Sat, 02 Jul 2016 21:08:02 +0200

php-horde-util (2.5.7-3) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Wed, 08 Jun 2016 07:18:04 +0200

php-horde-util (2.5.7-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Sun, 13 Mar 2016 15:07:20 +0100

php-horde-util (2.5.7-1) unstable; urgency=medium

  * New upstream version 2.5.7

 -- Mathieu Parent <sathieu@debian.org>  Wed, 03 Feb 2016 22:31:54 +0100

php-horde-util (2.5.6-2) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 21:47:47 +0200

php-horde-util (2.5.6-1) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf
  * New upstream version 2.5.6

 -- Mathieu Parent <sathieu@debian.org>  Mon, 10 Aug 2015 00:10:35 +0200

php-horde-util (2.5.5-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 2.5.5

 -- Mathieu Parent <sathieu@debian.org>  Tue, 05 May 2015 09:56:02 +0200

php-horde-util (2.5.1-5) unstable; urgency=medium

  * Fix autopkgtest tests:
    - Depends on locales
    - generate some locales (and thus needs-root)
    - run tests with LANG=cs_CZ.UTF-8
    - run tests as www-data (to drop unnecessary privileges)

 -- Mathieu Parent <sathieu@debian.org>  Thu, 16 Oct 2014 21:37:44 +0200

php-horde-util (2.5.1-4) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 13:41:16 +0200

php-horde-util (2.5.1-3) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 12:53:56 +0200

php-horde-util (2.5.1-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 21:49:42 +0200

php-horde-util (2.5.1-1) unstable; urgency=medium

  * New upstream version 2.5.1

 -- Mathieu Parent <sathieu@debian.org>  Sun, 17 Aug 2014 21:54:07 +0200

php-horde-util (2.4.0-1) unstable; urgency=medium

  * New upstream version 2.4.0

 -- Mathieu Parent <sathieu@debian.org>  Sat, 17 May 2014 13:12:46 +0200

php-horde-util (2.3.0-1) unstable; urgency=low

  * New upstream version 2.3.0

 -- Mathieu Parent <sathieu@debian.org>  Sun, 11 Aug 2013 12:48:28 +0200

php-horde-util (2.2.2-2) unstable; urgency=low

  * Use pristine-tar

 -- Mathieu Parent <sathieu@debian.org>  Thu, 06 Jun 2013 09:29:02 +0200

php-horde-util (2.2.2-1) unstable; urgency=low

  * New upstream version 2.2.2

 -- Mathieu Parent <sathieu@debian.org>  Tue, 14 May 2013 21:00:24 +0200

php-horde-util (2.2.1-1) unstable; urgency=low

  * New upstream version 2.2.1

 -- Mathieu Parent <sathieu@debian.org>  Sun, 07 Apr 2013 18:16:48 +0200

php-horde-util (2.0.3-1) unstable; urgency=low

  * New upstream version 2.0.3

 -- Mathieu Parent <sathieu@debian.org>  Thu, 10 Jan 2013 22:46:33 +0100

php-horde-util (2.0.2-2) unstable; urgency=low

  * Add a description of Horde in long description
  * Updated Standards-Version to 3.9.4, no changes
  * Replace horde4 by PEAR in git reporitory path
  * Fix Horde Homepage
  * Remove debian/pearrc, not needed with latest php-horde-role

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Jan 2013 21:31:46 +0100

php-horde-util (2.0.2-1) unstable; urgency=low

  * New upstream version 2.0.2

 -- Mathieu Parent <sathieu@debian.org>  Fri, 04 Jan 2013 19:17:23 +0100

php-horde-util (2.0.1-1) unstable; urgency=low

  * New upstream version 2.0.1
  * Fixed watchfile
  * Updated Standards-Version to 3.9.3: no change
  * Updated copyright format URL
  * Updated debian/pearrc to install Horde apps in /usr/share/horde
    instead of /usr/share/horde4
  * Updated Homepage
  * Added Vcs-* fields

 -- Mathieu Parent <sathieu@debian.org>  Fri, 30 Nov 2012 22:12:46 +0100

php-horde-util (1.1.0-1) unstable; urgency=low

  * Horde_Util package.
  * Initial packaging (Closes: #635821)
  * Copyright file by Soren Stoutner and Jay Barksdale

 -- Mathieu Parent <sathieu@debian.org>  Tue, 10 Jan 2012 22:37:36 +0100
